<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Users extends Model
{
    use HasFactory;
    protected $table = 'users';
    protected $fillable = [
        'first_name',
        'email',
        'phone',
        'last_name',
        'profession',
        'dob',
    ];

    public function checkUser($email)
	{   
        /**
         * Check product variation exist or not if exist return productId else null   
         *  
         */
		$email = strtoupper(trim($email)); 
		$variant = Users::select('id')->where(DB::raw('upper(email)'),$email)->first();
	
		if($variant)
		{ 
			return true; 
		}
		else
		{
			return null;
		}
    }
}
 