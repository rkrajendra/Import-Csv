<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Users;

class CsvImportController extends Controller
{
    //
    public function show()
    { 
        $users = Users::get();
        return view('users.show', compact('users'));
    } 
    public function store(Request $request)
    {
        $rules = array(
            'user_file' => 'required',
            'user_file' => 'required|max:10000|mimes:csv,CSV',
        ); 
        $file = $request->file('user_file');
        $data = array_map('str_getcsv', file($file->getRealPath()));
        $filename = $file->getClientOriginalName();
        if(!empty($filename)){ 
            $date = date('Y-m-d-His');
            $file->storeAs('/public/users/'.$date.'/', $filename);
            $productfile = url('/').'/storage/users/'.$date.'/'. $filename;   
        }   
        $sucess = array();
        $users_data = array_slice($data, 1); 
        if (sizeof($users_data) > 0) {
            foreach ($users_data as $key => $data) {
                $user = new Users;
                if (!empty(trim($data[1]))) {
                    if (!empty(trim($data[3]))) {
                        $product_variant = $user->checkUser($data[3]);
                        if ($product_variant) {
                            $array = array('status'=>0,'index'=>$data[1],'message'=>'Ignored due to already exist');
                            array_push($sucess,$array);
                            continue;
                        } else {
                            $date = strtotime($data[5]);
                            $user->first_name   = $data[1];
                            $user->last_name    = $data[2];
                            $user->email        = $data[3];
                            $user->phone        = $data[4];
                            $user->dob          = date('Y-m-d', $date);
                            $user->profession   = $data[6];
                            if ($user->save()) {
                                echo "success";
                                $array=array('status'=>1,'index'=>$data[1],'message'=>'User inserted');
                                array_push($sucess,$array);
                                continue;
                            } else {
                                $array=array('status'=>0,'index'=>$data[1],'message'=>' ignored
                                due to validation failure');
                                array_push($sucess,$array);
                                continue;
                            }
                        }
                    }else{
                        $array=array('status'=>0,'index'=>$key,'message'=>'Email ignored
                        due to validation failure');
                        array_push($sucess,$array);
                        continue;
                    }
                }else{
                    $array=array('status'=>1,'index'=>$key,'message'=>'First Name should be not empty');
                    array_push($sucess,$array);
                    continue;
                }
            }
        }
        return redirect()->back()->with('message',$sucess); 
    }
}
