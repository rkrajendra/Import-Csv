<?php

use App\Http\Controllers\CsvImportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashtemp.index');
    // return view('welcome');
})->name('dashboard');
Route::get('/users',  [CsvImportController::class, 'show'] )->name('users'); 
//products import route
Route::post('/users/import',  [CsvImportController::class, 'store'] )->name('importfile');