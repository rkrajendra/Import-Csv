@extends('layouts.sidebar')
@section('title','Products List')
@section('contents')

<div class="row">
    <div class="col-md-12">
        @if(session()->has('message'))
        <div id="successMessage" style="text-align:center; width:100%">
            <div class="alert alert-success">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#INDEX</th>
                            <th>Message</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(session()->get('message') as $key=>$message)
                        <tr>
                            <td>
                                @if($message['status'] == 0)
                                <spanc class="text-danger">{{$message['index']}}</span>
                                    @else
                                    <spanc class="text-success">{{$message['index']}}</span>
                                        @endif
                            </td>
                            <td>
                                @if($message['status'] ==0)
                                <spanc class="text-danger">{{$message['message']}}</span>
                                    @else
                                    <spanc class="text-success">{{$message['message']}}</span>
                                        @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        <div class="card card-rounded">
            <div class="card-body">
                <div class="d-sm-flex justify-content-between align-items-start">
                    <div>
                        <h4 class="card-title card-title-dash">Users</h4>
                    </div>
                    <div id="performance-line-legend">
                        <button type="button" class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal">
                            Add User
                        </button>
                    </div>
                </div>
                <table class="table" id="users">
                    <thead>
                        <tr>
                            <th>#INDEX</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Date of Birth</th>
                            <th>Profession</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($users->isNotEmpty())
                        @foreach($users as $key=>$user)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->phone}}</td>
                            <td>{{$user->dob}}</td>
                            <td>{{$user->profession}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>





    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Users</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('importfile')}}" onsubmit="return myFunction()" enctype="multipart/form-data">
                        @csrf
                        <input name="user_file" id="user_file" type="file" önchange="checkfile(this);" accept=".csv" />
                        <input type="submit" />
                        <br>
                    </form>

                </div>
                <span id="errormsg" class="text-danger"></span>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    @endsection
    <!-- Include css of drage style -->
    @push('css')
    <link rel="stylesheet" href="{{asset('dash/css/dragstyle.css')}}">
    @endpush
    <!-- Include js datatable-->
    @push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    @endpush
    <!-- Include js drag file script-->
    @push('scripts')
    <script type="text/javascript" src="{{asset('dash/js/dragescript.js')}}"> </script>
    @endpush